#ifndef RECORDTHREAD_H
#define RECORDTHREAD_H

#include <QObject>
#include <QDebug>
#include <QThread>
extern "C"
{
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libavfilter/avfilter.h>
    #include <libavdevice/avdevice.h>
}
class RecordThread : public QThread
{
    Q_OBJECT
public:
    explicit RecordThread(QObject *parent = nullptr);
    void setFlag(bool flag=true);
    AVCodecContext * open_encoder();
    void encode(AVCodecContext *c_ctx,AVFrame *frame,AVPacket *pkt,FILE* output);
    AVFrame * creat_frame();
protected:
    void run();
signals:
    void isDone();

private:
    bool isStop=false;
};

#endif // RECORDTHREAD_H
