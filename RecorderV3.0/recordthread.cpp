#include "recordthread.h"

RecordThread::RecordThread(QObject *parent) : QThread(parent)
{

}
void RecordThread::run()
{
    //很复杂的数据处理
    //需要耗时较长
    int ret = 0;
    char errors[1024] = {0};
    //context
    AVFormatContext *fmt_ctx = NULL;   //ffmpeg下的“文件描述符”

    //paket
    int count = 0;
    AVPacket packet;

    char *devicename =  "hw:0";
    //register audio device
    avdevice_register_all();

    //get format
    AVInputFormat *iformat = av_find_input_format("Alsa");

    //open audio
    if( (ret = avformat_open_input(&fmt_ctx, devicename, iformat, NULL)) < 0)
    {
        av_strerror(ret, errors, 1024);
        printf("Failed to open audio device, [%d]%s\n", ret, errors);
        return;
    } else {
        qDebug()<<"success\n";
    }


    av_init_packet(&packet);
    //create file
    char *out = "/opt/audi.aac";
    FILE *outfile = fopen(out,"wb+");
    //打开编码器上下文
    AVCodecContext* c_ctx = open_encoder();
    //音频输入数据
    AVFrame *frame = av_frame_alloc();
    frame->nb_samples = 512;
    frame->format=AV_SAMPLE_FMT_S16;
    frame->channel_layout=AV_CH_LAYOUT_STEREO;
    av_frame_get_buffer(frame,0);

    AVPacket *newpkt = av_packet_alloc();//分配编码后的数据空间
    if(!newpkt)
    {
    }

    int swr_num = 0;
    // 虚拟机环境，依然需要临时缓冲区存放采集到的数据，采集一定量的数据之后再给编码器进行编码
    uint8_t *bufferData = new uint8_t[2048];
    // 从设备读取数据
    while ((ret = av_read_frame(fmt_ctx, &packet) == 0) && isStop==false)
    {
        av_log(NULL, AV_LOG_INFO, "Packet size: %d(%p), count = %d\n",
               packet.size, packet.data, count++);

        if (swr_num < 1984)
        {
            for (int i = 0; i < packet.size; ++i)
            {
                bufferData[i + swr_num] = packet.data[i];
            }
            swr_num += packet.size;
        }
        else
        {
            for (int i = 0; i < packet.size; ++i)
            {
                bufferData[i + swr_num] = packet.data[i];
            }
            swr_num = 0;
            // 将采集到的音频数据放入frame缓冲区中，如果采集到的数据格式和编码器相同，那么不需要进行重采样
            // 我的设备采集参数和fdk_aac编码器需要的参数相同，所以不用进行重采样，采集到的音频数据可以直接发送给编码器进行编码
            memcpy((void *)frame->data[0], bufferData, 2048);

            ret = avcodec_send_frame(c_ctx,frame);
            while(ret>=0) {//ret>=0 说明数据设置成功
                ret = avcodec_receive_packet(c_ctx,newpkt);//获取编码好的音频，如果成功，重复获取，直到失败为止
                if(ret <0) {
                    if((ret == AVERROR(EAGAIN)) || (ret==AVERROR_EOF)){
                        break;
                    } else {
                        qDebug()<<"error ,encoding audio frame\n";
                        exit(-1);
                    }
                }
                fwrite(newpkt->data,1,newpkt->size,outfile);
                fflush(outfile);
            }


        }

        // 释放packet空间
        av_packet_unref(&packet);
    }
    // 释放AVFrame 和 AVPacket
    av_frame_free(&frame);
    av_packet_free(&newpkt);
    av_free(c_ctx);

    fclose(outfile);
    avformat_close_input(&fmt_ctx);//releas ctx
    emit isDone();
}
AVCodecContext* RecordThread::open_encoder()
{
    // 创建编码器
    // avcodec_find_encoder(AV_CODEC_ID_AAC); // 使用ID创建编码器
    AVCodec *codec = avcodec_find_encoder_by_name("libfdk_aac"); // 按name创建编码器，按需要修改编码器类型
    // 创建codec上下文
    if(codec==NULL) {
        qDebug()<<"failed to open encoder\n";
    }
    AVCodecContext *codec_ctx = avcodec_alloc_context3(codec);
    codec_ctx->sample_fmt = AV_SAMPLE_FMT_S16;       // 输入音频的采样大小。fdk_aac需要16位的音频输													                入数据
    codec_ctx->channel_layout = AV_CH_LAYOUT_STEREO; // 输入音频的CHANNEL LAYOUT
    codec_ctx->channels = 2;                         // 输入音频的声道数
    codec_ctx->sample_rate = 44100;                  // 输入音频的采样率
    codec_ctx->bit_rate = 0;                         // AAC : 128K   AAV_HE: 64K  AAC_HE_V2: 32K. bit_rate为0时才会查找profile属性值
    codec_ctx->profile = FF_PROFILE_AAC_HE;
    // 打开编码器
    if (avcodec_open2(codec_ctx, codec, NULL) < 0)
    {
        qDebug()<<"failed to open aac\n";
        return NULL;
    }
    return codec_ctx;
}
AVFrame * RecordThread::creat_frame()
{
    // 音频输入数据
    AVFrame *frame = av_frame_alloc();
    if(!frame)
    {
    }

    frame->nb_samples     = 512;                   // 单通道一个音频帧的采样数
    frame->format         = AV_SAMPLE_FMT_S16;     // 采样大小
    frame->channel_layout = AV_CH_LAYOUT_STEREO;   // channel layout

    av_frame_get_buffer(frame, 0);
    if (!frame->buf[0])
    {
    }

}

void RecordThread::encode(AVCodecContext *ctx,AVFrame *frame,AVPacket *pkt,FILE* output)
{
    int ret = 0;
        // 将数据发送给编码器
        ret = avcodec_send_frame(ctx, frame);
        if(ret<0) qDebug()<<"写不进去\n";
        while (ret >= 0)
        {
            // 获取编码后的音频数据
            ret = avcodec_receive_packet(ctx, pkt);
            if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
            {
                return;
            }
            else if (ret < 0)
            {
                printf("Error, encoding audio fail");
                exit(-1);
            }
            // 写入文件
            fwrite(pkt->data, 1, pkt->size, output);
            fflush(output);
        }
        return;

}

void RecordThread::setFlag(bool flag)
{
    isStop = flag;
}
