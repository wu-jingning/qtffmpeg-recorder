#include "widget.h"
#include "ui_widget.h"
#include <QFileDialog>
#include <QMessageBox>
#include<QTime>
#include<QTimer>
QString srcDirPath;
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    num =  0;
    // ui->lineEdit->setText(QString::number(num));
    ui->lcdNumber->setDigitCount(8);
    QTime time;
    time.setHMS(0,0,0);
    ui->lcdNumber->display(time.toString("hh:mm:ss"));
    //这个this是为了指定父对象，
    //只要指定了父对象，那么在堆区申请的空间，会在释放父对象的时候，会自动释放
    timer = new QTimer(this);
    //只要设定的时间到，timer 就会产生一个timeout的信号，而且是循环产生
    connect(timer,SIGNAL(timeout()),this,SLOT(update_timer()));


    //分配空间
    pthread = new RecordThread(this);

    //主线程回收子线程资源
    connect(pthread,&RecordThread::isDone,this,&Widget::dealDone);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::dealDone()
{
    on_buttonStop_clicked();
}

void Widget::on_buttonStart_clicked()
{

    if(srcDirPath=="") {
        QMessageBox::warning(NULL, "warning", "请设置文件保存路径！\n", QMessageBox::Yes, QMessageBox::Yes);
    } else {
        if(timer->isActive() == false) {
            ui->buttonStart->setText("正在录制");
            timer->start(10);
            pthread->start();
        }
    }
}


void Widget::on_buttonStop_clicked()
{
    if(timer->isActive() == true) {
        timer->stop();
    }
    ui->buttonStart->setText("开始录制");
    if(pthread->isRunning() == false)
    {
        return;
    }
    pthread->setFlag(true);
    pthread->quit();
    pthread->wait();
    if(srcDirPath!="") srcDirPath="";
}


void Widget::on_pushButton_clicked()
{
    QString runPath = QCoreApplication::applicationDirPath();
    srcDirPath = QFileDialog::getSaveFileName(
                this, "choose src Directory",runPath,"AAC文件(*.aac)",nullptr,QFileDialog::DontConfirmOverwrite);


}

void Widget::update_timer()
{
    num++;
    QTime time(0,0,0);
    QTime t = time.addSecs(num);
    ui->lcdNumber->display(t.toString("hh:mm:ss"));
}
