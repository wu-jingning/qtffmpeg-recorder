#include "recordthread.h"
#include <QTextCodec>
#include <QMessageBox>
extern QString srcDirPath;

RecordThread::RecordThread(QObject *parent) : QThread(parent)
{

}
void RecordThread::run()
{
    rec_audio();//录制线程主函数
}
void RecordThread::rec_audio()
{
    char* out;//设置文件保存路径，需要转换一下格式变成c语言类型的
    qDebug()<<srcDirPath;
    QByteArray ba = srcDirPath.toLatin1(); // must
    out=ba.data();
    //create file
    //char *out = "/opt/xxx.aac";
    FILE *outfile = fopen(out,"wb+");

    //paket
    int ret=0;
    int count = 0;
    AVPacket packet;
    av_init_packet(&packet);

    //重采样缓冲区
    uint8_t **src_data = NULL;
    int src_linesize = 0;
    uint8_t **dst_data =NULL;
    int dst_linesize=0;

    AVFormatContext* fmt_ctx=open_dev();//打开设备
    AVCodecContext* c_ctx = open_encoder();//打开编码器上下文

    //音频输入数据
    AVFrame *frame = creat_frame();//创建编码器缓冲区
    AVPacket *newpkt = av_packet_alloc();//分配编码后的数据空间


    int swr_num = 0;// 虚拟机环境，需要临时缓冲区采集一定量的数据之后再给编码器进行编码，因为单通道采样大小至少为32，pktsize为64 64/2(2通道)/2(16bit)<32
    SwrContext* swr_ctx = init_swr();

    uint8_t *bufferData = new uint8_t[2048];
    alloc_data_resample(&src_data,&src_linesize,&dst_data,&dst_linesize);//分配重采样缓冲区


    while ((ret = av_read_frame(fmt_ctx, &packet) == 0) && isStop==false)// 从设备读取数据
    {
        av_log(NULL, AV_LOG_INFO, "Packet size: %d(%p), count = %d\n",
               packet.size, packet.data, count++);

        if (swr_num < 1984)
        {
            for (int i = 0; i < packet.size; ++i)
            {
                bufferData[i + swr_num] = packet.data[i];
            }
            swr_num += packet.size;
        }
        else
        {
            for (int i = 0; i < packet.size; ++i)
            {
                bufferData[i + swr_num] = packet.data[i];
            }
            swr_num = 0;
            qDebug()<<"重采样输入缓冲区大小 ="<<src_linesize;
            memcpy((void*)src_data[0],(void *)bufferData,2048);//读取设备音频数据缓冲区拷贝入重采样输入缓冲区

            swr_convert(swr_ctx,
                        dst_data,
                        512,
                        (const uint8_t **)src_data,
                        512);
            qDebug()<<"重采样输出缓冲区大小 ="<<dst_linesize;
            memcpy((void *)frame->data[0], (void *)dst_data[0], dst_linesize);//重采样输出缓冲区拷贝入编码器输入缓冲区

            encode(c_ctx,frame,newpkt,outfile);


        }
        // 释放packet空间
        av_packet_unref(&packet);


    }
    encode(c_ctx,NULL,newpkt,outfile);
    // 释放AVFrame 和 newpkt
    if(frame){
        av_frame_free(&frame);
    }
    if(newpkt) {
        av_packet_free(&newpkt);
    }
    //释放输入输出缓冲区资源
    if(src_data){
        av_freep(&src_data[0]);
    }
    av_freep(&src_data);

    if(dst_data){
        av_freep(&dst_data[0]);
    }
    av_freep(&dst_data);
    //释放重采样上下文
    if(swr_ctx) {
        swr_free(&swr_ctx);
    }

    if(c_ctx) {
        av_free(c_ctx);
    }

    if(fmt_ctx){
        avformat_close_input(&fmt_ctx);//releas ctx
    }
    if(outfile){
        fclose(outfile);
    }
    av_log(NULL,AV_LOG_DEBUG,"finish!\n");


    emit isDone();//线程结束
}

AVFormatContext *RecordThread::open_dev()
{
    //很复杂的数据处理
    //需要耗时较长
    int ret = 0;
    char errors[1024] = {0};
    //context
    AVFormatContext *fmt_ctx = NULL;//ffmpeg下的“文件描述符”
    char *devicename =  "hw:0";
    //register audio device
    //avdevice_register_all();

    //get format
    AVInputFormat *iformat = av_find_input_format("alsa");

    //open audio
    if( (ret = avformat_open_input(&fmt_ctx, devicename, iformat, NULL)) < 0)
    {
        av_strerror(ret, errors, 1024);
        printf("Failed to open audio device, [%d]%s\n", ret, errors);
        return NULL;
    } else {
        qDebug()<<"success\n";
    }
    return fmt_ctx;
}

AVCodecContext* RecordThread::open_encoder()
{
    // 创建编码器
    // avcodec_find_encoder(AV_CODEC_ID_AAC); // 使用ID创建编码器
    AVCodec *codec = avcodec_find_encoder_by_name("libfdk_aac"); // 按name创建编码器，按需要修改编码器类型
    // 创建codec上下文
    if(codec==NULL) {
        qDebug()<<"failed to open encoder\n";
    }
    AVCodecContext *codec_ctx = avcodec_alloc_context3(codec);
    codec_ctx->sample_fmt = AV_SAMPLE_FMT_S16;       // 输入音频的采样大小。fdk_aac需要16位的音频输													                入数据
    codec_ctx->channel_layout = AV_CH_LAYOUT_STEREO; // 输入音频的CHANNEL LAYOUT
    codec_ctx->channels = 2;                         // 输入音频的声道数
    codec_ctx->sample_rate = 44100;                  // 输入音频的采样率
    codec_ctx->bit_rate = 0;                         // AAC : 128K   AAV_HE: 64K  AAC_HE_V2: 32K. bit_rate为0时才会查找profile属性值
    codec_ctx->profile = FF_PROFILE_AAC_HE;
    // 打开编码器
    if (avcodec_open2(codec_ctx, codec, NULL) < 0)
    {
        qDebug()<<"failed to open aac\n";
        return NULL;
    }
    return codec_ctx;
}
AVFrame * RecordThread::creat_frame()
{
    // 音频输入数据
    AVFrame *frame = av_frame_alloc();
    if(!frame)
    {
    }

    frame->nb_samples     = 512;                   // 单通道一个音频帧的采样数
    frame->format         = AV_SAMPLE_FMT_S16;     // 采样大小
    frame->channel_layout = AV_CH_LAYOUT_STEREO;   // channel layout

    av_frame_get_buffer(frame, 0);
    if (!frame->buf[0])
    {
        qDebug()<<"ERROR frame";
    }
    return frame;

}

SwrContext *RecordThread::init_swr()
{
    //重采样上下文
    SwrContext *swr_ctx = NULL;
    //channel, number/
    swr_ctx = swr_alloc_set_opts(NULL,                 //ctx
                                 AV_CH_LAYOUT_STEREO,  //输出channel布局
                                 AV_SAMPLE_FMT_S16,    //输出的采样格式
                                 44100,                 //输出采样率
                                 AV_CH_LAYOUT_STEREO,  //输入。。。
                                 AV_SAMPLE_FMT_S16,
                                 44100,
                                 0,NULL);

    if(swr_init(swr_ctx) < 0) {
        qDebug()<<"failed to open ctx";
    } else {  //等于0

    }
    return swr_ctx;
}

void RecordThread::alloc_data_resample(uint8_t ***src_data, int *src_linesize, uint8_t ***dst_data, int *dst_linesize)
{
    //创建输入缓冲区
    av_samples_alloc_array_and_samples(src_data,         //输入缓冲区地址
                                       src_linesize,     //缓冲区大小
                                       2,                 //通道个数
                                       512,               //单通道采样个数
                                       AV_SAMPLE_FMT_S16,
                                       0);
    //创建输出缓冲区
    av_samples_alloc_array_and_samples(dst_data,         //输出缓冲区地址
                                       dst_linesize,     //缓冲区大小
                                       2,                 //通道个数
                                       512,                 //单通道采样个数
                                       AV_SAMPLE_FMT_S16,//采样格式
                                       0);

}

void RecordThread::encode(AVCodecContext *ctx,AVFrame *frame,AVPacket *pkt,FILE* output)
{
    int ret = 0;
    // 将数据发送给编码器
    ret = avcodec_send_frame(ctx, frame);
    if(ret<0) qDebug()<<"写不进去\n";
    while (ret >= 0)
    {
        // 获取编码后的音频数据
        ret = avcodec_receive_packet(ctx, pkt);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
        {
            return;
        }
        else if (ret < 0)
        {
            qDebug()<<"Error, encoding audio fail";
            exit(-1);
        }
        // 写入文件
        fwrite(pkt->data, 1, pkt->size, output);
        fflush(output);
    }
    return;

}

void RecordThread::setFlag(bool flag)
{
    isStop = flag;
}

