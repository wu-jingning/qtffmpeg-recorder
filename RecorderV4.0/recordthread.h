#ifndef RECORDTHREAD_H
#define RECORDTHREAD_H

#include <QObject>
#include <QDebug>
#include <QThread>
extern "C"
{
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libavfilter/avfilter.h>
    #include <libavdevice/avdevice.h>
    #include <libswresample/swresample.h>
}
class RecordThread : public QThread
{
    Q_OBJECT
public:
    explicit RecordThread(QObject *parent = nullptr);

    void rec_audio();//录制音频主函数
    AVFormatContext* open_dev();//打开设备
    AVCodecContext * open_encoder();
    void encode(AVCodecContext *c_ctx,AVFrame *frame,AVPacket *pkt,FILE* output);
    AVFrame * creat_frame();
    SwrContext* init_swr();//创建重采样上下文
    void alloc_data_resample(uint8_t ***src_data,int *src_linesize,uint8_t ***dst_data,int *dst_linesize);
    void setFlag(bool flag=true);

protected:
    void run();
signals:
    void isDone();

private:
    bool isStop=false;
};

#endif // RECORDTHREAD_H
