# Qtffmpeg录音机

#### 介绍
Ubuntu18.04下qt5.9.9+ffmpeg4.3.2实现简易录音机
ffmpeg4.3.2
其他依赖库
lib_fdkaac模块
#### 版本说明
V1.0 采集原始数据并保存为pcm数据
V2.0 采集原始数据并重采样为位深f32le的pcm数据
V3.0 采集原始数据并通过AAC编码输出
V4.0 采集原始数据重采样并编码输出为aac文件，优化了界面


#### 使用

QT直接编译运行即可，V1.0-V3.0的Recordthread.cpp中可修改保存的文件路径
用ffplay播放 pcm文件用说明

#### 待改善




#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
