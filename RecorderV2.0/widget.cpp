 #include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //分配空间
    pthread = new RecordThread(this);

    //主线程回收子线程资源
    connect(pthread,&RecordThread::isDone,this,&Widget::dealDone);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::dealDone()
{
    on_buttonStop_clicked();
}

void Widget::on_buttonStart_clicked()
{
    ui->buttonStart->setText("正在录制");
    pthread->start();
}


void Widget::on_buttonStop_clicked()
{
    ui->buttonStart->setText("开始录制");
    if(pthread->isRunning() == false)
    {
        return;
    }
    pthread->setFlag(true);
    pthread->quit();
    pthread->wait();
}

