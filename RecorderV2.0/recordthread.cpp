#include "recordthread.h"

RecordThread::RecordThread(QObject *parent) : QThread(parent)
{

}
void RecordThread::run()
{

    int ret = 0;
    char errors[1024] = {0};


    //重采样缓冲区
    uint8_t **src_data = NULL;
    int src_linesize = 0;
    uint8_t **dst_data =NULL;
    int dst_linesize=0;

    //context
    AVFormatContext *fmt_ctx = NULL;   //ffmpeg下的“文件描述符”

    //paket
    int count = 0;
    AVPacket pkt;

    char *devicename =  "hw:0";
    //register audio device
    avdevice_register_all();

    //get format
    AVInputFormat *iformat = av_find_input_format("Alsa");

    //open audio
    if( (ret = avformat_open_input(&fmt_ctx, devicename, iformat, NULL)) < 0)
    {
        av_strerror(ret, errors, 1024);
        printf("Failed to open audio device, [%d]%s\n", ret, errors);
        return;
    } else {
        qDebug()<<"success\n";
    }


    av_init_packet(&pkt);
    //create file
    char *out = "/opt/0607.pcm";
    FILE *outfile = fopen(out,"wb+");



    SwrContext* swr_ctx = Init_swr();//初始化上下文函数
    //64/4=16/2=8
    //创建输入缓冲区
    av_samples_alloc_array_and_samples(&src_data,         //输入缓冲区地址
                                       &src_linesize,     //缓冲区大小
                                       2,                 //通道个数
                                       512,               //单通道采样个数
                                       AV_SAMPLE_FMT_S16,
                                       0);
    //创建输出缓冲区
    av_samples_alloc_array_and_samples(&dst_data,         //输出缓冲区地址
                                       &dst_linesize,     //缓冲区大小
                                       2,                 //通道个数
                                       512,                 //单通道采样个数
                                       AV_SAMPLE_FMT_S16,//采样格式
                                       0);
    //read data form audio
    int swr_num = 0;
    uint8_t *bufferData = new uint8_t[2048];
    while((ret = (av_read_frame(fmt_ctx, &pkt))) == 0 && isStop==false)
    {
        av_log(NULL, AV_LOG_INFO, "pkt size is %d（%p）, count=%d\n",
               pkt.size,pkt.data, count++);
        if(swr_num < 1984) {

            for(int i = 0;i<pkt.size;++i) {
                bufferData[i+swr_num]=pkt.data[i];
            }
            swr_num+=pkt.size;

        } else {
            for(int i=0;i<pkt.size;++i) {
                bufferData[i+swr_num]=pkt.data[i];
            }
            swr_num=0;
            //内存拷贝，按字节拷贝
            memcpy((void *)src_data[0],(void *)bufferData,2048);

            //重采样
            swr_convert(swr_ctx,
                        dst_data,  //输出结果缓冲区
                        512,          //输出每个通道的采样数
                        (const uint8_t **)src_data,//输入缓冲区
                        512);         //输入单个通道的采样数

            //write file
            //fwrite(pkt.data,  pkt.size, 1,outfile);
            fwrite(dst_data[0],1,dst_linesize,outfile);
            fflush(outfile);
        }
        av_packet_unref(&pkt);//release pkt
    }
    //内存拷贝，按字节拷贝
    memcpy((void *)src_data[0],(void *)bufferData,swr_num);

    //重采样
    swr_convert(swr_ctx,
                dst_data,  //输出结果缓冲区
                512,          //输出每个通道的采样数
                (const uint8_t **)src_data,//输入缓冲区
                512);         //输入单个通道的采样数

    //write file
    //fwrite(pkt.data,  pkt.size, 1,outfile);
    fwrite(dst_data[0],1,swr_num,outfile);
    fflush(outfile);
    //close file
    fclose(outfile);

    //释放输入输出缓冲区资源
    if(src_data){
        av_freep(&src_data[0]);
    }
    av_freep(&src_data);

    if(dst_data){
        av_freep(&dst_data[0]);
    }
    av_freep(&dst_data);
    //释放重采样上下文
    swr_free(&swr_ctx);
    avformat_close_input(&fmt_ctx);//releas ctx
    emit isDone();
}
SwrContext* RecordThread::Init_swr(){
    //重采样上下文

    SwrContext *swr_ctx = NULL;
    //channel, number/
    swr_ctx = swr_alloc_set_opts(NULL,                 //ctx
                                 AV_CH_LAYOUT_STEREO,  //输出channel布局
                                 AV_SAMPLE_FMT_S16,    //输出的采样格式
                                 44100,                 //输出采样率
                                 AV_CH_LAYOUT_STEREO,  //输入。。。
                                 AV_SAMPLE_FMT_S16,
                                 44100,
                                 0,NULL);

    if(swr_init(swr_ctx) < 0) {
        qDebug()<<"failed to open ctx";
    } else {  //等于0

    }
    return swr_ctx;
}
void RecordThread::setFlag(bool flag)
{
    isStop = flag;
}
