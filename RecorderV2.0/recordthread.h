#ifndef RECORDTHREAD_H
#define RECORDTHREAD_H

#include <QObject>
#include <QDebug>
#include <QThread>
extern "C"
{
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libavfilter/avfilter.h>
    #include <libavdevice/avdevice.h>
    #include <libswresample/swresample.h>
    #include <string.h>
}
class RecordThread : public QThread
{
    Q_OBJECT
public:
    explicit RecordThread(QObject *parent = nullptr);
    void setFlag(bool flag=true);

protected:
    void run();
    SwrContext* Init_swr();
signals:
    void isDone();

private:
    bool isStop;
};

#endif // RECORDTHREAD_H
